# Introduction to docker

## Content :
### Example 1 - Simple application in the container with the Dockerfile :
* [How-To](example1/README.md)
### Example 2 - Modification of existing image :
* [TODO](example2/README.md)
### Example 3 - Application with DB :
* [How-To](example3/README.md)
### Example 4 - Application with DB [But better :)] :
* [How-To](example4/README.md)