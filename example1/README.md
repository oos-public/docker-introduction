# Simple Dockerfile

```
FROM alpine:edge - important to define the base (can be pure OS or already created app)
LABEL MAINTAINER="nikolas.lucansky@gmail.com" - Label definition, can be used for kubernetes with labels etc ...
HEALTHCHECK NONE - Define the healthcheck for the container

RUN mkdir /fileinput - RUN exec command on host
COPY config.cfg /fileinput/config.cfg - COPY the artifact between host / image [ADD - have the download capability]

RUN mkdir /example
ONBUILD RUN echo "HI" > /example/file.txt - ONBUILD is a special function ... its skipped on a first build, but when the image is rebuild second time ... its executed.
eg. DevOps will create base image for devs and they just rebuild it with /src :)

WORKDIR /example - self explainatory
```