# Simple Go app with MySQL DB

## How-To :

* Run `docker build -t go_app:latest .`
* Run `docker run --name mysql --network local_test -e MYSQL_ROOT_PASSWORD="<DB_PWD>" -d mysql:latest`
  * Please notice the password field!
* Run `docker exec -it mysql bash`
  * CREATE DATABASE test;<br>
    USE test;<br>
    CREATE TABLE test (id INT AUTO_INCREMENT PRIMARY KEY, x_value VARCHAR(255));
* Run `docker run -d --name go_app --network local_test -p 9999:9999 -e DB_CONN_STRING="root:<DB_PWD>@tcp(mysql:3306)/test" go_app:latest`
  * Please notice the password field in the connection string must match the DB password!





[mysql]  [go_app]
 |          |
 _ _ _local_test