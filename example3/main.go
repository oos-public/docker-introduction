package main

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"fmt"
	"html"
	"log"
	"net/http"
	"os"
)

func insert(db *sql.DB, value string) (int64, error) {
	stmt, err := db.Prepare("INSERT test SET x_value=?")
	if err != nil {
		return -1, err
	}
	defer stmt.Close()

	res, err := stmt.Exec(value)
	if err != nil {
		return -1, err
	}
	return res.LastInsertId()
}

func main() {
	var xDbString string
	//export DB_CONN_STRING="root:aegeechuchieghahweegh3quechaireM@tcp(127.0.0.1:3306)"
	//export DB_WCONN_STRING="root:aegeechuchieghahweegh3quechaireM@tcp(127.0.0.1:3306)/test"
	xDbString = os.Getenv("DB_CONN_STRING")
	var DB_DSN = xDbString

	http.HandleFunc("/", func(write http.ResponseWriter, request *http.Request) {
		fmt.Fprint(write, "Hello, ", html.EscapeString(request.URL.Path))
		// Insert data
		xData := html.EscapeString(request.URL.Path)
		println("Inserting ... " + xData)

		db, err := sql.Open("mysql", DB_DSN)
		if err != nil {
			log.Fatal("Cannot open DB connection", err)
		}
		defer db.Close()

		id, err := insert(db, xData)
		if err != nil {
			log.Fatal("Failed to insert into database", err)
		}
		log.Printf("Inserted row with ID of: %d\n", id)
	})

	http.HandleFunc("/favicon.ico", func(write http.ResponseWriter, request *http.Request) {
		fmt.Fprint(write, "favicon")
	})

	log.Fatal(http.ListenAndServe(":9999", nil))
}

