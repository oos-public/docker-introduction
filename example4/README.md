# Simple Go app with MySQL DB | Better one :)

## How-To :

* Build go_app and customMySQL images
  * Run `docker network create local_test`
  * Run `cd go_app && docker build -t go_app:latest .`
  * Run `cd mysql && docker build -t customdb:latest .`
* Run `docker run --name mysql --network local_test -e MYSQL_ROOT_PASSWORD="<DB_PWD>" -d customdb:latest`
  * Please notice the password field!
* Run `docker volume create mysqldb`
* Run `docker run -d --name go_app -v mysqldb:/var/lib/mysql --network local_test -p 9999:9999 -e DB_CONN_STRING="root:<DB_PWD>@tcp(mysql:3306)/test" go_app:latest`
  * Please notice the password field in the connection string must match the DB password!


